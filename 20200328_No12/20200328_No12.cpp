﻿// 20200328_No12.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include "BaseBall.h"

bool CheckInputValidation(int a, int b, int c);

int main()
{
	int a = 0, b = 0, c = 0;

	while (true) {
		cout << "0~9중 서로다른 숫자 숫자 3개를 입력해주세요 : ";
		cin >> a;
		cin >> b;
		cin >> c;
		if (CheckInputValidation(a,b,c)) break;
		else {
			cout << "0~9중 서로다른 숫자 숫자를 입력해주세요" << endl;
			system("pause");
			system("cls");
		}
	}
	BaseBall baseBall(a, b, c);
	system("cls");
	while (true)
	{
		cout << "숫자 3개를 맞춰보세요 : ";
		cin >> a;
		cin >> b;
		cin >> c;
		if (CheckInputValidation(a, b, c)){
			if (baseBall.Check(a,b,c)) break;
		}
		else {
			cout << "0~9중 서로다른 숫자 숫자를 입력해주세요" << endl;
			continue;
		}
	}

	system("pause");
	return 0;
	
}

bool CheckInputValidation(int a, int b, int c) {
	if (0 <= a && a <= 9 &&
		0 <= b && b <= 9 &&
		0 <= c && c <= 9 &&
		a != b && b != c && c != a) return true;
	return false;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
