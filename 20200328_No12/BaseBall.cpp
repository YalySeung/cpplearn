#include "pch.h"
#include "BaseBall.h"

int BaseBall::GetStrikeCnt(int _a, int _b, int _c)
{
	int ret = 0;

	if (a == _a) ret++;
	if (b == _b) ret++;
	if (c == _c) ret++;

	return ret;
}

int BaseBall::GetBallCnt(int _a, int _b, int _c)
{
	int ret = 0;

	if (a == _b || a == _c) ret++;
	if (b == _a || b == _c) ret++;
	if (c == _a || c == _b) ret++;

	return ret;
}

void BaseBall::Print(int strike, int ball)
{
	if (strike == 0 && ball == 0) cout << "아웃!!" << endl;
	else if (strike == 3) cout << "정답입니다." << endl;
	else if (strike == 0) cout << ball << "볼ㅜㅜ" << endl;
	else if (ball == 0) cout << strike << "스트라잌!!!" << endl;
	else cout << "==> "<< strike << "스트라잌!!! " << ball << "볼ㅜㅜ" << endl;
}

BaseBall::BaseBall(int _a, int _b, int _c)
{
	a = _a;
	b = _b;
	c = _c;
}

bool BaseBall::Check(int _a, int _b, int _c)
{
	int strike = 0 , ball = 0;
	strike = GetStrikeCnt(_a, _b, _c);
	ball = GetBallCnt(_a, _b, _c);
	Print(strike, ball);

	if (strike == 3) return true;
	return false;

}
