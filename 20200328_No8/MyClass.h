#pragma once
enum Weight{Much = 0, Ordinary, Small};
class Bon;
class Dog
{
	char* name;
	int age;
public :
	Dog(const char* _name, int _age);
	~Dog();
	void PrintAge();
	void PrintStr(Bon* bon);
};

class Bon {
	Weight weight;
	const char* type[10] = { "많이", "적당히", "적게" };
public:
	Bon(Weight _weight);
	friend class Dog;
};

