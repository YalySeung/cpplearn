#include "pch.h"
#include "NameCard.h"

NameCard::NameCard(const char* _name, const char* _phoneNo, const char* _address, Rank _rank)
{
	name = new char[strlen(_name) + 1];
	phoneNo = new char[strlen(_phoneNo) + 1];
	address = new char[strlen(_address) + 1];
	strcpy_s(name, strlen(_name) + 1, _name);
	strcpy_s(phoneNo, strlen(_phoneNo) + 1, _phoneNo);
	strcpy_s(address, strlen(_address) + 1, _address);
	rank = _rank;
}

NameCard::~NameCard()
{
	cout << "소멸자 호출 !!" << endl;
	if (name) {
		delete[] name;
		name = NULL;
	}
	if (address) {
		delete[] address;
		address = NULL;
	}
	if (phoneNo) {
		delete[] phoneNo;
		phoneNo = NULL;
	}
}

void NameCard::PrintNameCard()
{
	printf("이름 : %s\n전화번호 : %s\n주소 : %s\n직급 : %s\n", name, phoneNo, address, rankStr[rank]);
}
