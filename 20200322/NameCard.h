#pragma once


enum Rank{ 
	employee = 0,
	substitute,
	Exaggeration,
	Director,
	RankSize
};


class NameCard {
	const char* rankStr[RankSize] = { "employee", "substitute", "Exaggeration", "Director" };
	char* name;
	char* phoneNo;
	char* address;
	Rank rank;

public:
	NameCard(const char* name, const char* _phoneNo, const char* _address, Rank _rank);
	~NameCard();

	void PrintNameCard();
};