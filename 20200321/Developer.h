#pragma once
#include "Human.h"

class Developer : public Human {
private:

public :
	Developer(int age, const char* name);
	void goToWork();
	void conference(); 
	void develop();
	void nightShift();
	void midnightSnack();
	void hobby();
};