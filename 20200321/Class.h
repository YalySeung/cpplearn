#pragma once
class A {
public: 
	void over();
};

class B : public A{
public:
	void over();
};


class Parent {
public:
	virtual void func();
};

class Child : public Parent {
public :
	virtual void func();
};

class ParentOne {
public :
	void funcone();
};

class ParentTwo {
public:
	void funcTwo();
};

class ChildOne :public ParentOne, public ParentTwo {
public :
	void func();
};