#include "pch.h" 
#include "MyClass.h"

MyClass::MyClass(const char* _name, int _age, int _studentID)
{
	cout << "생성자 호출" << endl;
	strcpy_s(name, sizeof(name), _name);
	age = _age;
	studentID = _studentID;
}

void MyClass::SetInfo(const char * _name, int _age, int _studentID)
{
	strcpy_s(name, sizeof(name), _name);
	age = _age;
	studentID =_studentID;
}

void MyClass::GetInfo()
{
	cout << "이름: " << name << endl;
	cout << "나이: " << age << endl;
	cout << "학번: " << studentID << endl;
}
