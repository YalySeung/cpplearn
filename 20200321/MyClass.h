#pragma once

class MyClass {
private:
	char name[10];
	int age;
	int studentID;
public :
	MyClass(const char* _name, int _age, int _studentID);
	~MyClass() {};
	void SetInfo(const char* _name, int _age, int _studentID);
	void GetInfo();
};