#include "pch.h"
#include "Human.h" 
#include "Developer.h"

Developer::Developer(int age, const char * name) : Human(age, name)
{

}

void Developer::goToWork()
{
	cout << "출근!" << endl;
}

void Developer::conference()
{
	cout << "회의!" << endl;
}

void Developer::develop()
{
	cout << "개발!" << endl;
}

void Developer::nightShift()
{
	cout << "야근!" << endl;
}

void Developer::midnightSnack()
{
	cout << "야식!" << endl;
}

void Developer::hobby()
{
	cout << "취미활동!" << endl;
}
