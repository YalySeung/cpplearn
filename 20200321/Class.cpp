#include "pch.h";
#include "Class.h";

void A::over()
{
	cout << "A 클래스의 over 함수 호출!" << endl;
}

void B::over()
{
	//A::over();
	cout << "B 클래스의 over 함수 호출!" << endl;
}

void Parent::func()
{
	cout << "부모 클래스의 func 함수 호출 !!" << endl;
}

void Child::func()
{
	cout << "자식 클래스의 func 함수 호출 !!" << endl;
}

void ParentOne::funcone()
{
	cout << "ParentOne 클래스에서 funcone() 호출 !" << endl;
}

void ParentTwo::funcTwo()
{
	cout << "ParentTwo 클래스에서 functwo() 호출 !" << endl;
}

void ChildOne::func()
{
	funcone();
	funcTwo();
}
