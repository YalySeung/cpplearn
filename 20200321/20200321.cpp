﻿// 20200321.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include "Developer.h"
#include "MyClass.h"
#include "Class.h"


bool isInValue(int min, int max, int input);

int main()
{
	//int inputValue = 0;
	//Developer dp(20, "김개발");
	//while (true)
	//{
	//	cout << "현재시간을 입력하세요(0~23)" << endl;
	//	cin >> inputValue;
	//	if (cin.fail())
	//	{
	//		cin.clear();
	//		cin.ignore(256, '\n');
	//	}
	//	else if (0 <= inputValue && inputValue <= 23)
	//	{
	//		if (isInValue(0, 2, inputValue))
	//		{
	//			dp.hobby();
	//		}
	//		else if (isInValue(2, 8, inputValue))
	//		{
	//			dp.sleep();
	//		}
	//		else if (isInValue(8, 9, inputValue))
	//		{
	//			dp.breakfast();
	//		}
	//		else if (isInValue(9, 10, inputValue))
	//		{
	//			dp.conference();
	//		}
	//		else if (isInValue(10, 13, inputValue))
	//		{
	//			dp.conference();
	//		}
	//		else if (isInValue(13, 14, inputValue))
	//		{
	//			dp.luncheon ();
	//		}
	//		else if (isInValue(14, 19, inputValue))
	//		{
	//			dp.develop();
	//		}
	//		else if (isInValue(10, 20, inputValue))
	//		{
	//			dp.dinner();
	//		}
	//		else if (isInValue(20, 22, inputValue))
	//		{
	//			dp.nightShift();
	//		}
	//		else if (isInValue(22, 24, inputValue))
	//		{
	//			dp.midnightSnack();
	//		}
	//	}
	//	cout << "==========================" << endl << endl;
	//}

	//MyClass my[5];
	//char name[10];
	//int age;

	//for (int i = 0; i < 5; i++)
	//{
	//	cin >> name >> age;
	//	my[i].SetInfo(name, age, i + 1);
	//}
	//for (int i = 0; i < 5; i++)
	//{
	//	my[i].GetInfo();
	//}

	//MyClass my[3] = { MyClass("김철수", 14, 1), MyClass("김영희", 14, 2),  MyClass("이영수", 14, 3) };

	//for (int i = 0; i < 3; i++)
	//{
	//	my[i].GetInfo();
	//}

	//MyClass * my[3];
	//char name[10];
	//int age, studentID;
	//for (int i = 0; i < 3; i++)
	//{
	//	cin >> name >> age >> studentID;
	//	my[i] = new MyClass(name, age, studentID);
	//}
	//for (int i = 0; i < 3; i++)
	//{
	//	my[i]->GetInfo();
	//	delete my[i];
	//}

	//B b, *pb = NULL;
	//A *pa = NULL;

	//pa = pb = &b;
	//pa -> over();
	//pb -> over();
	////b.over();

	//Parent P, *pP;
	//Child C;
	//pP = &P;
	//pP->func();
	//pP = &C;
	//pP->func();

	Child child;
	child.func();

	system("pause");
	return 0;
}

bool isInValue(int min, int max, int input) {
	if (min <= input && input < max)
	{
		return true;
	}
	return false;
}


// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
