﻿// 20200404.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"

string ReplaceString(string subject, const string &search, const string &replace);
void ReplaceStringInPlace(string& subject, const string& search, const string& replace);
bool Predicate(int n);

int main()
{
	//string myString = "abcd";
	//cout << myString.c_str() << endl;
	//myString = 'z';
	//cout << myString.c_str() << endl;
	//myString += 'x';
	//cout << myString.c_str() << endl;	
	//myString.append("appended!");
	//cout << myString.c_str() << endl;

	//cout << myString.size() << endl; //메모리 사이즈
	//cout << myString.length() << endl; //문자열 길이

	//cout << myString.capacity() << endl; 
	//cout << myString.max_size() << endl;

	//string base;
	//cout << base.capacity() << endl;
	//base = "hello world!"; //16bit
	//cout << base.capacity() << endl; 
	//base += "3456"; //32bit
	//cout << base.capacity() << endl;
	//base += "3456123123123123123"; //48bit
	//cout << base.capacity() << endl; //현재 문자열에 할당된 길이	
	//cout << base.max_size() << endl; //최대메모리사이즈
	//cout << base.at(0) << endl;
	//cout << base.find("ld") << endl; //문자 위치 찾기
	//cout << base.find("aaa") << endl; //문자 위치 찾기

	//string a = "string";
	//string b = "string";

	//if (a.compare(b) == 0)
	//{
	//	cout << "두 문자열이 같습니다." << endl;
	//}
	//else if (a.compare(b) < 0)
	//{
	//	cout << "a가 b보다 사전순으로 앞입니다." << endl;
	//}
	//else if (a.compare(b) > 0)
	//{
	//	cout << "b가 a보다 사전순으로 앞입니다." << endl;
	//}

	//문자열 변환
	//string s;
	//int i = 10;
	//s = to_string(i);
	//cout << s + s << endl;
	//
	//s = "123";
	//i = stoi(s);
	//cout << i + i << endl;

	//string input = "Hello C++!!";
	//cout << "Input string : " << input.c_str() << endl;
	//cout << "ReplaceString() 반환값 : " << ReplaceString(input, "lo", "!!").c_str() << endl;
	//cout << "ReplaceString() input String 안바뀜 : " << input.c_str() << endl;

	//ReplaceStringInPlace(input, "lo", "??");
	//cout << "ReplaceString() input String 바뀜 : " << input.c_str() << endl;

	//string str1 = "나는 C++을 배우고 있습니다.";
	//str1 += "\n나는 열심히 하고 있는 걸까요?";
	//cout << str1.c_str() << endl;
	//cout << "총 문자열의 길이는 : " << str1.length() << endl;

	////나는 지우기
	//int index = 0 ;
	//string strTemp = "나는 ";
	//while (true)
	//{
	//	index = str1.find(strTemp);
	//	if (index < 0) break;

	//	str1.replace(index, strTemp.length(), "");
	//}
	//cout << str1.c_str() << endl;

	//=========================================
	//list<int> it;
	//it.push_back(10);
	//it.push_back(20);
	//it.push_back(30);
	//it.push_back(40);
	//it.push_back(50);

	//list<int>::iterator iter;
	//for (iter = it.begin(); iter != it.end(); ++iter)
	//{
	//	cout << *iter << endl;
	//}
	//cout << endl;

	//iter = it.begin();
	//iter++;
	//iter++;

	////erase 삭제 
	//list<int>::iterator iter2 = it.erase(iter);
	//for (iter = it.begin(); iter != it.end(); ++iter)
	//{
	//	cout << *iter << endl;
	//}

	//cout << endl;
	//cout << "iter2 : " << *iter2 << endl;

	//it.push_back(10);
	//it.push_back(10);

	//for (iter = it.begin(); iter != it.end(); ++iter)
	//{
	//	cout << *iter << endl;
	//}

	//cout << endl;

	////리스트에서 원소 10 제거
	//it.remove(10);
	//
	//for (iter = it.begin();iter != it.end() ;++iter)
	//{
	//	cout << *iter << endl;
	//}
	//cout << endl;

	//// Predicate 함수에 해당하는 원소 제거 (30보다 크다)
	//it.remove_if(Predicate);

	//for (iter = it.begin(); iter != it.end(); ++iter)
	//{
	//	cout << *iter << endl;
	//}

	//cout << endl;
	//==================================

	vector<int> v;
	v.reserve(8); //벡터 메모리 공간 8 예약 할당

	v.push_back(10);
	v.push_back(20);
	v.push_back(30);
	v.push_back(40);
	v.push_back(50);

	for (vector<int>::size_type i = 0; i < v.size(); ++i) cout << v[i] << endl;
	cout << endl;
	cout << "size : " << v.size() << " capacity : " << v.capacity() << " maxSize : " << v.max_size() << endl << "-- resize(10) --" << endl;

	v.resize(10);

	for (vector<int>::size_type i = 0; i < v.size(); ++i) cout << v[i] << endl;

	cout << "size : " << v.size() << " capacity : " << v.capacity() << " maxSize : " << v.max_size() << endl << "-- resize(3) --" << endl;

	v.resize(3);

	for (vector<int>::size_type i = 0; i < v.size(); ++i) cout << v[i] << endl;

	cout << "size : " << v.size() << " capacity : " << v.capacity() << " maxSize : " << v.max_size() << endl << "-- vector clear() --" << endl;
	v.clear();

	if (v.empty())
	{
		cout << "벡터에 원소가 없다." << endl;
	}

	cout << "size : " << v.size() << " capacity : " << v.capacity() << " maxSize : " << v.max_size() << endl;

	system("pause");
	return 0;
}

bool Predicate(int n) {
	return n >= 30;
}

//원본 문자열에서는 아무 영향 x, 변;경된 문자열은 함수의 반환값으로 돌아온다.
string ReplaceString(string subject, const string &search, const string &replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != string::npos) //npos = nullPos
	{
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
	return subject;
}

//원본 문자열을 수정한다. 속도가 우선일경우 사용
void ReplaceStringInPlace(string& subject, const string& search, const string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != string::npos)
	{
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
