#include "pch.h"
#include "Dog.h"

using namespace std;

void Dog::SetInfo(const char* _name, int _age, Size _size) {
	sprintf_s(name, sizeof(name), _name);
	age = _age;
	size = _size;
}

void Dog::Howl() {
	char howlString[20];

	if (age < 5)
	{
		switch (size)
		{
		case Biggest:
			sprintf_s(howlString, sizeof(howlString), "����");
			break;
		case Big:
			sprintf_s(howlString, sizeof(howlString), "ůů");
			break;
		case Normal:
			sprintf_s(howlString, sizeof(howlString), "����");
			break;
		case Small:
			sprintf_s(howlString, sizeof(howlString), "�п�");
			break;
		case VerySmall:
			sprintf_s(howlString, sizeof(howlString), "�˾�");
			break;
		default:
			sprintf_s(howlString, sizeof(howlString), "�۸�");
			break;
		}
	}
	else {
		switch (size)
		{
		case Biggest:
			sprintf_s(howlString, sizeof(howlString), "��������");
			break;
		case Big:
			sprintf_s(howlString, sizeof(howlString), "ůůůů");
			break;
		case Normal:
			sprintf_s(howlString, sizeof(howlString), "��������");
			break;
		case Small:
			sprintf_s(howlString, sizeof(howlString), "�ппп�");
			break;
		case VerySmall:
			sprintf_s(howlString, sizeof(howlString), "�˾˾˾�");
			break;
		default:
			sprintf_s(howlString, sizeof(howlString), "�۸۸۸�");
			break;
		}
	}

	cout << "������ " << name << "�� " << age << "�� �Դϴ�." << endl << name << "�� ¢�� ��" << howlString << "�ϰ� ��ϴ�."<< endl;
}