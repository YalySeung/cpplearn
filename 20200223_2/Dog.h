#pragma once


enum Size {
	Biggest = 0,
	Big,
	Normal,
	Small,
	VerySmall
};

class Dog {
	char name[20];
	int age;
	Size size;
	
public	:
	void SetInfo(const char* name, int age, Size size);
	void Howl();
};