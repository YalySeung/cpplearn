#pragma once

class Human {
	char* _name;
	int _age;
	bool _isMan;
	char _strGender[10];

public :
	Human();
	Human(const char * name, int age, bool isMan);
	~Human();

	Human(const char* aStr); 
	Human(const Human& hm);
	



	void ShowInfo();
	void InitInfo(const char* name, int age, bool isman);
	void Showlaughter();


	void ChangeGender(const char* changeGender);
	void ChangeName(const char* changeName);

	const char* GetGender();
	char* GetName();
};
