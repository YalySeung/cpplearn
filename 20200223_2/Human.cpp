#include "pch.h"
#include "Human.h"

using namespace std;

Human::Human() {
	cout << "생성자가 호출" << endl;
	//ZeroMemory(_name, sizeof(_name));
	_name = NULL;
	_age = 0;
	_isMan = false;
	ZeroMemory(_strGender, sizeof(_strGender));
}

Human::Human(const char * name, int age, bool isMan) {
	//sprintf_s(_name, sizeof(_name), name);
	int len = strlen(name) + 1;
	_name = new char[len];
	sprintf_s(_name, sizeof(len), name);
	_age = age;
	_isMan = isMan;

	if (isMan) sprintf_s(_strGender, sizeof(_strGender), "남자");
	else sprintf_s(_strGender, sizeof(_strGender), "여자");
}

Human::~Human()
{
	cout << "소멸자 호출 !!!!" << endl;
	if (_name) delete[] _name;
	system("pause");
}

Human::Human(const char * aStr)
{
	_name = new char[strlen(aStr) + 1];
	sprintf_s(_name, sizeof(_name), aStr);

}

Human::Human(const Human & hm)
{
	_name = new char[strlen(hm._name) + 1];
	sprintf_s(_name, sizeof(_name),	hm._name);
}


void Human::ShowInfo() {

	cout << "이름: " << _name << ", 나이: " << _age << _strGender << endl;
}

void Human::InitInfo(const char* name, int age, bool isMan) {
	int len = strlen(name) + 1;
	if (_name) delete[] _name;
	_name = new char[len];
	sprintf_s(_name, sizeof(len), name);
	_age = age;
	_isMan = isMan;

	if (_isMan) sprintf_s(_strGender, sizeof(_strGender), "남자");
	else sprintf_s(_strGender, sizeof(_strGender), "여자");
	
}

void Human::Showlaughter() {
	if (_isMan) cout << "하하하하" << endl;
	else cout << "호호호호" << endl;
}


#pragma region Setter
void Human::ChangeName(const char * changeName)
{
	strcpy_s(_name, sizeof(_name), changeName);
}

void Human::ChangeGender(const char * changeGender)
{
	const char * male = "남자";
	for (int i = 0; *(changeGender + i) != NULL; i++)
	{
		if (*(changeGender + i) != *(male + i)) {
			_isMan = false;
		}
	}
}
#pragma endregion


#pragma region Getter
char * Human::GetName()
{
	return _name;
}

const char * Human::GetGender()
{
	return _isMan == true ? "남자" : "여자";
}
#pragma endregion





