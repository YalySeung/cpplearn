#pragma once
class Human
{

private:
	int _age;
	char _name[10];
public:

	Human(int age, const char* name);
	void breakfast();
	void luncheon();
	void dinner();
	void showInfo();
};

