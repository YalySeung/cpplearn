#pragma once
class FriendB;
class FriendA;

class FriendA
{

private :
	int data;
public :
	FriendA(int _data);
	void ShowData(FriendB b);
	friend class FriendB;
};

class FriendB {
private:
	int data;
public:
	FriendB(int _data);
	void ShowData(FriendA a);
	friend class FriendA;

};

