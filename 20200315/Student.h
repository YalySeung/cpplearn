#pragma once

class Student : public Human
{
private:
	char _school[30];
public:
	Student(int age, const char* name, const char* school);
	void schoolInfo();
};

