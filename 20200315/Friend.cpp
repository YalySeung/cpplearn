
#include "pch.h"
#include "Friend.h"

FriendA::FriendA(int _data)
{
	data = _data;
}

void FriendA::ShowData(FriendB b)
{
	cout << "b.data: " << b.data << endl;
}

FriendB::FriendB(int _data)
{
	data = _data;
}

void FriendB::ShowData(FriendA a)
{
	cout << "a.data: " << a.data << endl;
}
