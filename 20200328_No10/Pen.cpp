#include "pch.h"
#include "Pen.h"

Pen::Pen(const char * _name, Materials _material)
{
	int len = strlen(_name) + 1;
	name = new char[len];
	sprintf_s(name, len, _name);

	material = _material;
}

Pen::~Pen()
{
	if (name != NULL) {
		delete[] name;
		name = 0;
	}
}

void Pen::SetMat(Materials _material)
{
	material = _material;
}

void Pen::SetName(const char * _name)
{
	if (name != NULL) {
		delete[] name;
		name = 0;
	}
	int len = strlen(_name) + 1;
	name = new char[len];
	sprintf_s(name, len, _name);
}
