#pragma once
#include "Ink.h";

enum Materials {����ƽ, ����};

class Pen : public Ink
{
protected:
	char* name;
	Materials material;
	const char* materialStr[10] = { "����ƽ", "����" };
public :
	Pen(const char* _name, Materials _material);
	~Pen();
	void SetMat(Materials _material);
	void SetName(const char* _name);
};

