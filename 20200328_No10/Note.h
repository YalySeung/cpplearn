#pragma once
#include "Pen.h"

class Note : public Pen
{
	int wholeLineCnt;
	int currentLineCnt;
public :
	Note(const char* _name, Materials _material, int _wholeLineCnt, int _currentLineCnt);
	void SetLine(int _currentLineCnt);
	void DisPlay();
};

