#include "pch.h"
#include "Note.h"

Note::Note(const char * _name, Materials _material, int _wholeLineCnt, int _currentLineCnt):Pen(_name, _material)
{
	wholeLineCnt = _wholeLineCnt;
	currentLineCnt = _currentLineCnt;
}

void Note::SetLine(int _currentLineCnt)
{
	currentLineCnt = _currentLineCnt;
}

void Note::DisPlay()
{
	cout << "총 길이가 " << wholeLineCnt << "줄인 공책의 " << currentLineCnt << "번째 줄에 양이 " << amountStr[amount] << "인 " << colorStr[color] << "의 " << materialStr[material] << "싸인팬으로 글을 쓴다" << endl;
}
