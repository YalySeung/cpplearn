#pragma once

enum Colors{Red, Black};
enum Amounts {조금, 적당히, 많이};

class Ink
{
protected:
	Colors color;
	Amounts amount;
	const char* colorStr[10] = { "빨간색", "검은색" };
	const char* amountStr[10] = { "조금", "적당히", "많이" };
public:
	void SetColor(Colors _color);
	void SetMuch(Amounts _amount);
};

