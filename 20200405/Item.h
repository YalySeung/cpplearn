#pragma once

#include "pch.h"

enum Items {
	BronzeSword,
	Railroad,
	LeatherArmor,
	SkyShoes,
	Corner,
	ItemCnt
};

class Item
{
private :
	Items name;
	int price;
	int rank;
public :
	void PrintInfo();
	void SetInfo(Items _name, int _price, int _rank);
	string GetItemName(Items itemName);
};

