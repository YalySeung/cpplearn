﻿// 20200405_No4.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"

int main()
{
	vector<string> stringList;
	string stringTemp;
	int inputCnt;
	int retCnt = 0;

	cout << "문장 개수를 입력하세요" << endl;

	cin >> inputCnt;
	cin.ignore(256, '\n');

	for (int i = 0; i < inputCnt; i++)
	{
		getline(cin ,stringTemp);
		stringList.push_back(stringTemp);
	}

	cout << "검사할 문장 입력" << endl;

	getline(cin, stringTemp);

	//문장 검사
	for (int i = 0; i < stringList.size(); i++)
	{
		string currentSring = stringList[i];
		while (true) {
			int index = currentSring.find(stringTemp);
			if (index >= 0)
			{
				currentSring.replace(index, stringTemp.length(), "");
				retCnt++;
			}
			else break;
		}
	}

	cout << "============Show List============" << endl;
	for (int i = 0; i < stringList.size(); i++)
	{
		cout << stringList[i] << endl;
	}

	cout << endl << "일치하는 문장의 갯수는 : " << retCnt << endl;

	system("pause");
	return 0;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
