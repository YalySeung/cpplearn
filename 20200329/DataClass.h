#pragma once
#include "stdafx.h"

template<typename T>
class DataClass
{
private :
		T* _dataC;
public :
	DataClass(T* dataC, int count);
	~DataClass();
	void ShowInfo();
};

template<typename T>
inline DataClass<T>::DataClass(T* dataC, int count)
{
	_dataC = new T[count];
	_dataC = dataC;
}

template<typename T>
inline DataClass<T>::~DataClass()
{
	int dataSize = sizeof(*_dataC) / sizeof(T);
	if (_dataC != NULL) delete _dataC;

}

template<typename T>
inline void DataClass<T>::ShowInfo()
{
	int dataSize = sizeof(*_dataC) / sizeof(T);
	for (int i = 0; i < dataSize; i++)
	{
		cout << _dataC[i] << endl;
	}
}
