#include "stdafx.h"
#include "Vector2.h"

Vector2::Vector2(float _x, float _y):x(_x), y(_y)
{
}

Vector2 Vector2::operator+(Vector2 & ref)
{
	return Vector2(x + ref.x, y + ref.y);
}

Vector2 Vector2::operator-(Vector2 & ref)
{
	return Vector2(x - ref.x, y - ref.y);
}

Vector2 Vector2::operator*(float num)
{
	return Vector2(x * num, y * num);
}

void Vector2::ShowPos()
{
	cout << "x : " << x << ", y : " << y << endl;
}

Vector2 operator*(float num, Vector2 ref)
{
	ref.x *= num;
	ref.y *= num;
	return ref;
}
