﻿// 20200329.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "stdafx.h"
#include "NUMBOX.h"
#include "Student.h"
#include "Vector2.h"
#include "DataClass.h"

NUMBOX operator+(int num, NUMBOX ref);
NUMBOX operator*(int num, NUMBOX ref);
template<typename T>
void Swap(T& num1, T&num2);

int main()
{
	//NUMBOX n1(10, 20);
	//NUMBOX n2(20, 30);
	//NUMBOX newNumbox = n1 + n2;
	////NUMBOX newNumbox = n1.operator+(n2);
	//NUMBOX subNumbox = n1 - n2;

	//n1.ShowNumber();
	//n2.ShowNumber();
	//newNumbox.ShowNumber();
	//subNumbox.ShowNumber();

	//NUMBOX nb1(10, 20);
	//NUMBOX result = 10 + nb1 + 40;
	//result.ShowNumber();	
	//NUMBOX nb1(10, 20);
	//nb1.ShowNumber();
	//NUMBOX result = ++nb1;
	//result.ShowNumber();
	//result = nb1++;
	//result.ShowNumber();
	//nb1.ShowNumber();

	//Student st1("김철수", 14);
	//Student st2("이영희", 15);
	//st2 = st1;
	//st1.ShowInfo();
	//st2.ShowInfo();

	//Vector2 v1(1, 2);
	//Vector2 v2(4, 7);
	////Vector2 v3 = v1 + v2;
	////v3.ShowPos();
	////v3 = v1 - v2;
	////v3.ShowPos();
	////v3 = v1 * 3;
	////v3.ShowPos();	
	////v3 = 4.5 * v1;
	////v3.ShowPos();

	//Swap(v1, v2);
	//v1.ShowPos();
	//v2.ShowPos();

	//DataClass<int> data1(50);
	//data1.ShowInfo();
	//DataClass<char> data2('A');
	//data2.ShowInfo();
	//DataClass<float> data3(30.1231f);
	//data3.ShowInfo();
	int a[5] = { 1, 3, 4, 5, 6 };
	DataClass<int> data(a);

	data.ShowInfo();

	system("pause");
	return 0;
}

template<typename T>
void Swap(T& num1, T& num2) {
	T temp = num1;
	num1 = num2;
	num2 = temp;
}

NUMBOX operator+(int num, NUMBOX ref) {
	ref.num1 += num;
	ref.num2 += num;
	return ref;
}

NUMBOX operator*(int num, NUMBOX ref) {
	ref.num1 *= num;
	ref.num2 *= num;
	return ref;
}
// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
