#pragma once
class Vector2
{
private :
	float x;
	float y;
public:
	Vector2(float _x, float _y);
	Vector2 operator+(Vector2& ref);
	Vector2 operator-(Vector2& ref);
	Vector2 operator*(float num);
	friend Vector2 operator*(float num, Vector2 ref);
	void ShowPos();
};

