#include "pch.h"
#include "Grade.h"

Grade::Grade(char* _name)
{
	//동적할당
	int length = strlen(_name) + 1;
	name = new char[length];
	sprintf_s(name, length, _name);

}

Grade::~Grade()
{
	delete[] name;
}

void Grade::Init(int _kor, int _eng, int _math)
{
	kor = _kor;
	eng = _eng;
	math = _math;
	avr = (float)(kor + eng + math) / 3;
}

void Grade::PrintGrade()
{
	printf("이름 : %s\n국어 : %d점\n영어 : %d점\n수학 : %d점\n평균 : %f점\n", name, kor, eng, math, avr);
}
