#pragma once


class Grade {
private:
	char* name = 0;
	int kor = 0;
	int eng = 0;
	int math = 0;
	float avr = 0;

public:
	Grade(char* _name);
	~Grade();
	void Init(int _kor, int _eng, int _math);
	void PrintGrade();
};