#pragma once
#include "Person.h"
class Student : public Person
{
private :
	int student_id_;
public :
	Student(int sid, const char* name, int age);
	virtual void ShowPersonInfo();
};

