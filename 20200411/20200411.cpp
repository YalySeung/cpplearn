﻿// 20200411.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include "PersonInfo.h"

#define MAX_SIZE 1000
char inputString[MAX_SIZE];

PersonInfo GetPersonFromLine(string lineValue, int _number);

int main()
{
	////파일 입력(쓰기)
	//ofstream outFile("output.txt");
	//for (int i = 0; i < 10; i++)
	//{
	//	outFile << i << endl;
	//}
	//outFile.close();

	////파일 출력(읽기)
	//ifstream inFile("output.txt");
	//if (inFile.is_open())
	//{
	//	while (!inFile.eof())
	//	{
	//		//inFile.getline(inputString, 100);
	//		//cout << inputString << endl;
	//		cout << (char)inFile.get();
	//	}
	//	inFile.close();
	//}

	//int inputCnt = 0, age = 0;
	//string name;
	//float height = 0;

	//cout << "몇명의 정보를 입력하시겠습니까 : ";
	//cin >> inputCnt;
	//
	////파일 입력(쓰기)
	//ofstream outFile("infoData.txt");

	//for (int i = 0; i < inputCnt; i++)
	//{
	//	//입력
	//	cout << i + 1 << "번 이름 : ";
	//	cin >> name;
	//	cout << i + 1 << "번 나이 : ";
	//	cin >> age;
	//	cout << i + 1 << "번 키 : ";
	//	cin >> height;
	//	outFile << name << " " << age << " " << round(height * 100) / 100 << endl;
	//}

	//outFile.close();

	list<PersonInfo> personList;
	string stringTemp;
	int number = 0;

	ifstream inFile("infoData.txt");
	if (inFile.is_open())
	{
		while (!inFile.eof())
		{
			number++;
			inFile.get(inputString, 100, ' ');
			inFile.getline(inputString, 100);
			stringTemp = inputString;

			if (stringTemp.size() == 0) continue;
			
			personList.push_back(GetPersonFromLine(stringTemp, number));
		}

		list<PersonInfo>::iterator iter;
		for (iter = personList.begin(); iter != personList.end(); ++iter)
		{
			iter->PrintInfo();
		}
	}

	return 0;

}

//한 줄로부터 PersonInfo 클래스 생성
PersonInfo GetPersonFromLine(string lineValue, int _number) {
	string name;
	int age = 0;
	float height = 0;
	
	int preIndex;
	int postIndex;

	preIndex = 0;

	//데이터 분리
	//이름
	postIndex = lineValue.find(" ");
	if (preIndex >= 0)
	{
		name = lineValue.substr(0, postIndex);
		lineValue = lineValue.substr(postIndex + 1, lineValue.length() - (postIndex - preIndex));
		preIndex = postIndex + 1;
	}

	//나이
	postIndex = lineValue.find(" ");
	if (preIndex >= 0)
	{
		age = stoi(lineValue.substr(0, postIndex));
		lineValue = lineValue.substr(postIndex + 1, lineValue.length() - (postIndex - preIndex));
		preIndex = postIndex + 1;
	}

	//키
	postIndex = lineValue.find(" ");
	if (postIndex < 0)
	{
		height = stof(lineValue);
	}

	PersonInfo personInfoTemp = PersonInfo();
	personInfoTemp.InitInfo(name, age, height, _number);
	return personInfoTemp;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
