#pragma once
#include "pch.h"
class PersonInfo
{
private:

	int number;
	string name;
	int age;
	float height;

public:

	void InitInfo(string _name, int _age, float _height, int _number);

	void PrintInfo();
};

