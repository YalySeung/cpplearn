﻿// CppLearn.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"

enum eOddEven {
	All = 0,
	ODD,
	EVEN
};
float CalBmi(float length, float weight);

void PrintScreen();
void PrintScreen(int num);
void PrintScreen(float silsu);
void PrintScreen(int num, float silsu);
void PrintScreen(float silsu, int num);

int AddValue(int a, int b);
float AddValue(float a, float b);
float AddValue(int a, float b);
float AddValue(float a, float b);
char* AddValue(const char* a, const char* b);

void Sort(int* arr, int cnt, bool iseven = true);

int SumPrint(int min, int max, eOddEven type = All);
//int GetStringLength(const char* str);
//char* AddValue(const char* a, const char* b);

namespace A {
	void Add() {
		printf("A의 Add 호출 !!\n");
	}
}

namespace B {
	void Add() {
		printf("B의 Add 호출 !!\n");
	}
}


using namespace A;



string TypeText[3] = { "전체",  "홀수", "짝수" };

int main()
{
	//   cout << "Hello World!" << endl;
	   //cout << "Hello" << 1234 << endl;

	   //cout << setprecision(5) << 3.141597 << endl;
	   //cout << setprecision(5) << 975.6424 << endl;
	   //cout << setprecision(5) << 123.4567 << endl;

	   //자리수 설정
	   //cout << 986.123456 << endl;
	   //streamsize num = cout.precision(5);
	   //cout << 986.123456 << endl;
	   //cout.precision(num);
	   //cout << 987.123456 << endl;

	   //소수점 자리수 설정
	   //cout.setf(ios::fixed);
	   //cout.precision(1);
	   //cout << 987.654328 << endl;

	   //cout << 123456789 << endl;

	   //cout.setf(ios::fixed);
	   //cout.precision(2);
	   //cout << "이름 : " << "홍길동" << endl << "나이 : " << 20 << endl << "키   : " << 200.1123f << "cm" << endl;

	   //int num;
	   //char str[100];

	   //cout << "숫자와 이름을 입력하세요(000 ###) : ";
	   //cin >> num >> str;

	   //cout << "num : " << num << endl << "str : " << str << endl;

	   //char name[20] = {0,};
	   //float length = .0f;
	   //float weight = .0f;

	   //cout << "이름을 입력하세요 : ";
	   //cin >> name;
	   //cout << "키를 입력하세요 : ";
	   //cin >> length;
	   //cout << "몸무게를 입력하세요 : ";
	   //cin >> weight;

	   //cout.setf(ios::fixed);
	   //cout.precision(1);
	   //cout << name << "님의 비만도는 " << CalBmi(length, weight) << "입니다." << endl;

	   //Add();
	   //B::Add();
	   //PrintScreen();
	   //PrintScreen(123);
	   //PrintScreen(123.0123f);
	   //PrintScreen(123, 123.0123f);
	   //PrintScreen(123.0123f, 123);

	   //cout << AddValue("안녕", "친구") << endl;

	   //int numList[4] = { 1,64,75,6 };
	   //Sort(numList, 4);
	   //for (int i = 0; i < 4; i++)
	   //{
	   //	cout << "arr[" << i << "] = " << numList[i]<<endl;
	   //}

	int min = 0, max = 0, inputNum = 0;
	cout << "시작 정수를 입력하세요 : ";
	cin >> min;
	cout << "끝 정수를 입력하세요 : ";
	cin >> max;
	cout << "1.전체" << endl;
	cout << "2.홀수" << endl;
	cout << "3.짝수" << endl;
	cout << "번호를 입력하세요 : ";
	cin >> inputNum;

	//합 계산 함수
	SumPrint(min, max, eOddEven(inputNum - 1));


	system("pause");
	return 0;
}

//min부터 max 까지의 합 계산
int SumPrint(int min, int max, eOddEven type) {
	int sum = 0;
	switch (type)
	{
	case All:
		for (int i = min; i <= max; i++)
		{
			sum += i;
		}
		break;
	case ODD:
		for (int i = min; i <= max; i++)
		{
			if (i % 2 == 1)sum += i;
		}
		break;
	case EVEN:
		for (int i = min; i <= max; i++)
		{
			if (i % 2 == 0)sum += i;
		}
		break;
	default:
		break;
	}

	cout << min <<" ~ " << max << "까지의"  << "수합 : " << sum << endl;

	return 0;
}

float CalBmi(float length, float weight) {
	float bmi;
	bmi = weight / (length / 100 * length / 100);
	return bmi;
}

void PrintScreen() {
	cout << "Hello World" << endl;
}
void PrintScreen(int num) {
	cout << num << endl;
}

void PrintScreen(float silsu) {
	cout << silsu << endl;
}

void PrintScreen(int num, float silsu) {
	cout << num << ":" << silsu << endl;
}

void PrintScreen(float silsu, int num) {
	cout << silsu << ":" << num << endl;

}

int AddValue(int a, int b) {
	return a + b;
}

float AddValue(float a, int b) {
	return a + b;
}

float AddValue(int a, float b) {
	return a + b;
}

float AddValue(float a, float b) {
	return a + b;
}

char* AddValue(const char* a, const char* b) {
	static char text[200] = { 0, };
	int idx = 0;
	int cnt = 0;

	while (a[idx] != '\0')
	{
		text[cnt++] = a[idx++];
	}
	idx = 0;
	while (b[idx] != '\0')
	{
		text[cnt++] = b[idx++];
	}
	text[cnt] = '\n';
	return text;
}

void Sort(int* arr, int cnt, bool iseven) {
	for (int i = 0; i < cnt - 1; i++)
	{
		for (int j = i + 1; j < cnt; j++)
		{
			if (iseven)
			{
				if (arr[i] > arr[j])
				{
					int t = arr[i];
					arr[i] = arr[j];
					arr[j] = t;
				}
			}
			else {
				if (arr[i] < arr[j])
				{
					int t = arr[i];
					arr[i] = arr[j];
					arr[j] = t;
				}
			}
		}
	}
}

//
//char* AddValue(const char* a, const char* b) {
//	char* retStr = 0;
//	retStr = (char*)malloc(sizeof(char) * GetStringLength(a) * GetStringLength(b));
//
//	for (int i = 0; i < GetStringLength(a); i++)
//	{
//		*(retStr + i) = *(a + i);
//	}
//
//	for (int i = 0; i < GetStringLength(b); i++)
//	{
//		retStr[GetStringLength(a) + i] = b[i];
//	}
//
//	return retStr;
//}
//
//int GetStringLength(const char* str) {
//	int ret = 0;
//	while (true)
//	{
//		if (str[ret] == '\0')
//		{
//			return ret;
//		}
//		ret++;
//	}
//}


// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
